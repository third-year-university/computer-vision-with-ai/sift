import cv2
import matplotlib.pyplot as plt
import numpy as np

cam = cv2.VideoCapture(0)
cam.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1)
cam.set(cv2.CAP_PROP_EXPOSURE, 500)

showCrosshair = False
fromCenter = False

ret, face = cam.read()
face = cv2.cvtColor(face, cv2.COLOR_BGR2GRAY)

while cam.isOpened():

    ret, background = cam.read()
    background = cv2.cvtColor(background, cv2.COLOR_BGR2GRAY)

    orb = cv2.SIFT_create()

    key_points1, descriptors1 = orb.detectAndCompute(face, None)
    key_points2, descriptors2 = orb.detectAndCompute(background, None)

    matcher = cv2.BFMatcher()
    matches = matcher.knnMatch(descriptors1, descriptors2, k=2)

    best = []

    for m1, m2 in matches:
        if m1.distance < 0.75 * m2.distance:
            best.append([m1])

    if len(best) > 0:
        src_pts = np.float32([key_points1[m[0].queryIdx].pt for m in best]).reshape(-1, 1, 2)
        dst_pts = np.float32([key_points2[m[0].trainIdx].pt for m in best]).reshape(-1, 1, 2)
        M, hmask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
        h, w = face.shape
        pts = np.float32([[0, 0], [0, h - 1], [w - 1, h - 1], [w - 1, 0]]).reshape(-1, 1, 2)
        dst = cv2.perspectiveTransform(pts, M)
        result = cv2.polylines(background, [np.int32(dst)], True, 255, 3, cv2.LINE_AA)
    else:
        print("Suck!")
        mask = None

    matches_image = cv2.drawMatchesKnn(face, key_points1, background, key_points2,
                                       best, None)

    key = cv2.waitKey(100)

    if key == ord('1'):
        r = cv2.selectROI("selector", background, fromCenter, showCrosshair)
        print(r)
        face = background[r[1]:r[1] + r[3], r[0] : r[0] + r[2]]
        cv2.destroyWindow("selector")

    cv2.imshow("Camera", result)

# matches_image = cv2.drawMatches(single, key_points1, many, key_points2, best, None)
#
#
# plt.imshow(result)
# plt.show()

# plt.figure()
# plt.subplot(121)
# plt.imshow(single)
# plt.subplot(122)
# plt.figure()
# plt.imshow(matches_image)
# plt.show()
