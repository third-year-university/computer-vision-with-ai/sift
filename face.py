import cv2
import matplotlib.pyplot as plt
import numpy as np

conf = cv2.imread("images/solvay_conference.jpg")
cooper = cv2.imread("images/cooper.jpg")
deal_with = cv2.imread("images/dealwithit.png")

deal_with = deal_with[400:680, 260:1660]

face = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
eye = cv2.CascadeClassifier("haarcascade_eye.xml")
lbp_face = cv2.CascadeClassifier("lbpcascade_frontalface.xml")
glasses = cv2.CascadeClassifier("haarcascade_eye_tree_eyeglasses.xml")

min_x, min_y, max_x, max_y = 0, 0, 1, 1


def detector(img, classifier1, classifier2, scale=None, min_nbs=None):
    global deal_with
    global min_x
    global min_y
    global max_x
    global max_y
    result = img.copy()
    rects = classifier1.detectMultiScale(result, scaleFactor=scale, minNeighbors=min_nbs)
    rects2 = classifier2.detectMultiScale(result, scaleFactor=scale, minNeighbors=min_nbs)

    for (x, y, w, h) in rects2:
        cv2.rectangle(result, (x, y), (x + w, y + h), (255, 255, 255))

    for (x, y, w, h) in rects:
        cv2.rectangle(result, (x, y), (x + w, y + h), (255, 255, 255))
    if len(rects) == 2:
        print((rects[1][0] + rects[1][2] - rects[0][0], rects[1][1] + rects[1][3] - rects[0][1]))
        min_x = min(rects[0][0], rects[1][0])
        min_y = min(rects[0][1], rects[1][1])
        max_x = max(rects[0][0], rects[1][0]) + rects[0][2]
        max_y = max(rects[0][1], rects[1][1]) + rects[0][3]

        # print(print(deal_with))
    if len(rects2) > 0:
        deal_with_2 = cv2.resize(deal_with, (max_x - min_x, max_y - min_y))
        deal_with_2[deal_with_2 == (255, 255, 255)] = result[min_y:max_y, min_x:max_x, :][deal_with_2 == (255, 255, 255)]
        result[min_y:max_y, min_x:max_x, :] = deal_with_2
    return result


cam = cv2.VideoCapture(0)
cam.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1)
cam.set(cv2.CAP_PROP_EXPOSURE, 500)

while cam.isOpened():
    ret, img = cam.read()
    result = detector(img, glasses, face, 1.2, 3)
    cv2.imshow("Camera", result)
    key = cv2.waitKey(1)
    if key == ord('1'):
        cv2.destroyAllWindows()

# result = detector(conf, eye, 1.2, 5)
# plt.figure()
# plt.imshow(result)
#
# result = detector(cooper, eye, 1.2, 5)
#
# plt.figure()
# plt.imshow(result)
# plt.show()
