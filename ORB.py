import cv2
import matplotlib.pyplot as plt

single = cv2.imread("images/reeses_puffs.png")
many = cv2.imread("images/many_cereals.jpg")

orb = cv2.ORB_create()

key_points1, descriptors1 = orb.detectAndCompute(single, None)
key_points2, descriptors2 = orb.detectAndCompute(many, None)

# print(descriptors1)
# print(descriptors2)
#
# print(key_points1),
# print(key_points2)

matcher = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
matches = matcher.match(descriptors1, descriptors2)

matches = sorted(matches, key=lambda x: x.distance)

matches_image = cv2.drawMatches(single, key_points1, many, key_points2, matches, None)


plt.figure()
plt.subplot(121)
plt.imshow(single)
plt.subplot(122)
plt.figure()
plt.imshow(matches_image)
plt.show()