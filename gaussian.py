import cv2
import matplotlib.pyplot as plt
import numpy as np

single = cv2.imread("images/reeses_puffs.png", 0)
many = cv2.imread("images/many_cereals.jpg", 0)

orb = cv2.SIFT_create()

key_points1, descriptors1 = orb.detectAndCompute(single, None)
key_points2, descriptors2 = orb.detectAndCompute(many, None)

# print(descriptors1)
# print(descriptors2)
#
# print(key_points1),
# print(key_points2)

matcher = cv2.BFMatcher()
matches = matcher.knnMatch(descriptors1, descriptors2, k=2)


best = []

for m1, m2 in matches:
    if m1.distance < 0.75 * m2.distance:
        best.append([m1])

print(f"All matches {len(matches)}, best matches: {len(best)}")

if len(best) > 30:
    src_pts = np.float32([key_points1[m[0].queryIdx].pt for m in best]).reshape(-1, 1, 2)
    dst_pts = np.float32([key_points2[m[0].trainIdx].pt for m in best]).reshape(-1, 1, 2)
    M, hmask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
    h, w = single.shape
    pts = np.float32([[0, 0], [0, h - 1], [w - 1, h - 1], [w - 1, 0]]).reshape(-1, 1, 2)
    dst = cv2.perspectiveTransform(pts, M)
    result = cv2.polylines(many, [np.int32(dst)], True, 255, 3, cv2.LINE_AA)
else:
    print("Suck!")
    mask = None

matches_image = cv2.drawMatchesKnn(single, key_points1, many, key_points2,
                                   best, None)

# matches_image = cv2.drawMatches(single, key_points1, many, key_points2, best, None)
#
#
plt.imshow(result)
plt.show()

# plt.figure()
# plt.subplot(121)
# plt.imshow(single)
# plt.subplot(122)
# plt.figure()
# plt.imshow(matches_image)
# plt.show()