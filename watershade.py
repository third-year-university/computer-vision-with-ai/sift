import cv2
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import use


image = cv2.imread("images/pennies.jpg")
image1 = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
image2 = cv2.medianBlur(image1, 25)

_, thresh = cv2.threshold(image2, 160, 255, cv2.THRESH_BINARY_INV)

dist = cv2.distanceTransform(thresh, cv2.DIST_L2, 5)

_, fg = cv2.threshold(dist, 0.7*dist.max(), 255, 0)

fg = np.uint8(fg)

confuse = cv2.subtract(thresh, fg)

_, markers = cv2.connectedComponents(fg)
markers += 1
markers[confuse == 255] = 0

wmarkers = cv2.watershed(image, markers.copy())

contours, hierarchy = cv2.findContours(wmarkers, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)

for i in range(len(contours)):
    if hierarchy[0][i][3] == -1:
        cv2.drawContours(image, contours, i, (0, 255, 0), 10)

plt.subplot(121)
plt.imshow(image)
plt.subplot(122)
plt.imshow(wmarkers)
plt.show()